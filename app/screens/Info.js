import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, Linking } from 'react-native';
import NavigationBar from '../consts/NavigationBar.js';

export default function Info({ navigation }) {
    
    // Tästä eteenpäin ohjelma noudattaa pitkälti Favourites-näytön kommentteja
    return (
        
        <SafeAreaView style={styles.background}>
            <View style={styles.topView}>
                <TouchableOpacity onPress={() => navigation.navigate("Info")}>
                    <Image style={styles.icon} source={require('../assets/info.png')}/>
                </TouchableOpacity>
                    <Image style={styles.logo1} source={require('../assets/RGB_Oulu2026_gradient_horizontal.png')}/>
                <TouchableOpacity onPress={() => navigation.navigate("Profiili")}>
                    <Image style={styles.icon} source={require('../assets/profiili.png')}/>
                </TouchableOpacity>
            </View>
            <View style={styles.header}>
            <ScrollView>
              <Text style={styles.screenName}>Info</Text>
                <Text style={styles.headingName}>Mikä Oulu2026?</Text>
                <Text style={styles.bodyText}>Oulu valittiin vuoden 2026 Euroopan kulttuuripääkaupungiksi kansainvälisen asiantuntijaraadin toimesta vuonna 2021.</Text>
                <Image style={styles.image} source={require('../assets/Voitto-Oulu2026-1360x768_VERKKOON.jpg')}/>
                    <Text style={styles.headingName}>Mikä on kulttuuriohjelman tavoite?</Text>
                    <Text style={styles.bodyText}>
                        Kulttuuriohjelman keskeinen pääteema on kulttuuri-ilmastonmuutos, johon on sitoutunut Oulun lisäksi 38 pohjoisen Suomen kaupunkia ja kuntaa.
                        Oulu2026-alueen yhteisenä tavoitteena on luoda kulttuuria, hyvinvointia ja elinvoimaa pohjoiselle alueelle.{'\n'}{'\n'}
                        Kulttuuri-ilmastonmuutos luo inspiroivan tulevaisuuden pohjoiseen Eurooppaan kestävällä tavalla. Se nostaa taiteen ja kulttuurin merkitystä kaupunkien, kylien ja alueiden tulevaisuudessa.{'\n'}{'\n'}
                        Lue lisää Oulu2026-kulttuuriohjelmasta sekä -alueesta alla olevasta osoitteesta:
                    </Text>
                    {/* Linkkiä painamalla avautuu selaimeen Oulu2026-sivusto. */}
                    <Text style={styles.link}
                            onPress={() => Linking.openURL('https://oulu2026.eu')} >
                            {'oulu2026.eu'}
                    </Text>
                    <Image style={styles.image} source={require('../assets/pikisaari.jpg')}/>
                        <Text style={styles.headingName}>Mihin tätä sovellusta tarvitaan?</Text>
                        <Text style={styles.bodyText}>
                        Tämän sovelluksen tarkoituksena on toimia osana Oulu2026-kulttuuriohjelmaa ja esitellä Oulu2026-alueen kaupunkeja sekä niiden ruokakulttuuria.{'\n'}{'\n'}
                        Sovellus on toteutettu Oulun Chaîne des Rôtisseurs voutikunnan sekä Oulu2026-kulttuuriohjelman toimesta. 
                        </Text>
                        <Image style={styles.image} source={require('../assets/ruokakuva.jpeg')}/>
            </ScrollView>
            </View>
            <NavigationBar navigation={navigation}/>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: '#F6F7F1',
    },
    topView: {
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        position: 'absolute',
        backgroundColor: '#F6F7F1',
    },
    logo1: {
        width: 100,
        height: 47.29,
    },
    icon: {
        width: 30,
        height: 30,
    },
    header: {
        marginTop: 120,
        marginBottom: 60,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
    },
    screenName: {
        left: 27, 
        fontSize: 20, 
        paddingBottom: 40,
        fontWeight: 'bold', 
        color: '#001E60',
    },
    headingName: {
        fontSize: 20,
        paddingBottom: 20, 
        fontWeight: 'bold', 
        color: '#001E60',
        alignItems: 'center',
        textAlign: 'center',
    },
    bodyText: { 
        paddingBottom: 20,
        fontSize: 15, 
        fontWeight: 'regular', 
        color: '#001E60',
        alignSelf: 'center',
        textAlign: 'center',
        maxWidth: '80%',
    },
    link: {
        fontSize: 15,
        paddingBottom: 20,
        color: '#001E60',
        textAlign: 'center',
        textDecorationLine: 'underline',
    },
    image: {
        marginBottom: 30,
        height: 178,
        width: 312,
        borderRadius: 10,
        alignSelf: 'center',
    },

});
