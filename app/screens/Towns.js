import React, {useState} from 'react';
import { SafeAreaView, Text, View, StyleSheet, Image, FlatList, TouchableHighlight, TouchableOpacity } from 'react-native';
import kaupungit from '../consts/kaupungit.js';
import SearchBar from '../consts/SearchBar.js';
import NavigationBar from '../consts/NavigationBar.js';

export default function Towns({ navigation }) {

    const [searchPhrase, setSearchPhrase] = useState("");
    const [clicked, setClicked] = useState(false);

    const Card = ({ kaupunki }) => {

        return (
            // sama periaate kuin Recipies luokassa
            // importataan kaupungit rivillä 3 ja käytetään importtia flatlistin datalähteenä rivillä 62
            // card komponentin paramterin avulla näytetään kaikkien kaupunkien tiedot, jotka kaupunit muuttujasta löytyvät eli tässä tapauksessa 4 eri kaupunkia
                <TouchableHighlight
                    underlayColor={'#F6F7F1'}
                    activeOpacity={0.9}
                    onPress={() => navigation.navigate('Kaupunkisivu', kaupunki)}>
                    <View style={styles.card}>
                        <View style={{ alignItems: 'center'}}>
                            {/* haetaan kaupunkien kuvat kaupunki muuttujasta */}
                            <Image source={kaupunki.image} style={{ height: '85%', width: '100%', borderTopLeftRadius: 15, borderTopRightRadius: 15 }} />
                        </View>
                        <View style={{ marginHorizontal: 15, marginTop: -10 }}>
                            {/* haetaan kaupunkien nimet */}
                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#001E60' }}>{kaupunki.nimi}</Text>
                        </View>
                    </View>
                </TouchableHighlight>
        );
    };

    const renderItem = ({ item }) => {
        // when no input, show all
        if (searchPhrase === "") {
            return <Card kaupunki={item} />;
        }
        // filter of the name
        if (item.nimi.toUpperCase().includes(searchPhrase.toUpperCase().trim().replace(/\s/g, ""))) {
            return <Card kaupunki={item} />;
        }
    };

    // tästä eteenpäin koodi noudattaa pitkälti favourites luokan kommentteja
    return (
        <SafeAreaView style={styles.background}>
            <View style={styles.topView}>
                <TouchableOpacity onPress={() => navigation.navigate("Info")}>
                    <Image style={styles.icon} source={require('../assets/info.png')}/>
                </TouchableOpacity>
                    <Image style={styles.logo1} source={require('../assets/RGB_Oulu2026_gradient_horizontal.png')}/>
                <TouchableOpacity onPress={() => navigation.navigate("Profiili")}>
                    <Image style={styles.icon} source={require('../assets/profiili.png')}/>
                </TouchableOpacity>
            </View>
            <View style={styles.content} onStartShouldSetResponder={() => {setClicked(false);}}>
                <Text style={styles.screenName}>Kaupungit</Text>
                <SearchBar searchPhrase={searchPhrase} setSearchPhrase={setSearchPhrase} clicked={clicked} setClicked={setClicked}></SearchBar>
                <FlatList data={kaupungit} renderItem={renderItem} keyExtractor={(item) => item.id}></FlatList>
            </View>
            <NavigationBar navigation={navigation}/>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: '#F6F7F1',
    },
    topView: {
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        position: 'absolute',
        backgroundColor: '#F6F7F1',
    },
    logo1: {
        width: 100,
        height: 47.29,
    },
    icon: {
        width: 30,
        height: 30,
    },
    screenName: {
        left: 27,
        fontSize: 20, 
        fontWeight: 'bold', 
        color: '#001E60',
        marginBottom: 10,
    },
    content:{
        flex: 1,
        marginTop: 120,
        marginBottom: 60,
    },
    card: {
        height: 220,
        marginHorizontal: 20,
        marginBottom: 10,
        marginTop: 10,
        borderRadius: 15,
        elevation: 13,
        backgroundColor: '#ffff',
    },

});
