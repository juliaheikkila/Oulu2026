import React from 'react';
import MapView, { Marker } from 'react-native-maps';
import { StyleSheet, View } from 'react-native';
import kaupungit from '../consts/kaupungit.js';
import NavigationBar from '../consts/NavigationBar.js';

export default function Map({navigation}) {

    return (
        <View style={styles.container}>
        {/* Tuodaan kartta sovellukseen, joka valikoituu puhelimen käyttöjärjestelmän mukaan */}
            <MapView style={styles.map}
                   // Asetetaan kartta avautumaan aina Oulun alueelta
                   initialRegion={{
                       latitude: 65.012093,
                       longitude: 25.465076,
                       latitudeDelta: 0.0922,
                       longitudeDelta: 0.0421,
                   }}
               >
                   {kaupungit.map(kaupunki => (
                    // Asetetaan kartalle niiden paikkakuntien markerit, jotka löytyvät kaupungit.js tiedostosta
                       <Marker
                           key={kaupunki.id}
                           coordinate={{ latitude: kaupunki.latitude, longitude: kaupunki.longitude }}
                           // Markeria painamalla navigoidaan kyseisen kaupungin esittelysivulle
                           onPress={() => navigation.navigate('Kaupunkisivu', kaupunki)}
                       />
                   ))}
            </MapView>
            <NavigationBar navigation={navigation}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    map: {
        width: '100%',
        height: '100%',
    },

});
