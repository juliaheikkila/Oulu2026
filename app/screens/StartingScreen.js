import React from 'react';
import { Image, StyleSheet, View } from "react-native";


const StartingScreen = ({ navigation }) => {
    // asetetaan 3 sekunnin ajastin, jonka jälkeen siirrytään seuraavaan näkymään
    setTimeout(() => {
        navigation.navigate("Tervetuloa")
    }, 3000)

    return (
        <View
            style={styles.background}>
            <Image style={styles.logo1} source={require('../assets/RGB_Oulu2026_gradient_horizontal.png')} />
            <Image style={styles.logo2} source={require('../assets/Rotisseurs_logo.png')} />
        </View>
    );
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: '#F6F7F1',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo1: {
        width: 183,
        height: 87,
        margin: 20,
    },
    logo2: {
        width: 100,
        height: 100,
        margin: 20,
    },
})

export default StartingScreen;
