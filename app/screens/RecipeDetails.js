import React from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image, ScrollView, TouchableOpacity} from 'react-native';
import NavigationBar from '../consts/NavigationBar.js';

// Tuodaan funktion parametrissa navigation ja route, jotta käyttäjä voi navigoida näkymästä toiseen.
// Lisäksi yllä mainitun toimenpiteen avulla käyttäjälle näytetään sen reseptin kuvaus, minkä hän edellisessä vaiheessa valitsi.
export default function RecipeDetails({ navigation, route }) {
    // haetaan sen reseptin tiedot paramterista, jonka käyttäjä valitsi edellisessä näkymässä
    const item = route.params;

    return (
        // tämän alapuolella tuodaan sovelluksen yläosaan tarvitttavat ikonit ja kuvat. Voit katsoa tarkemmat kuvaukset Favourites-luokasta.
        <SafeAreaView style={styles.background}>
            <View style={styles.topView}>
                <TouchableOpacity onPress={() => navigation.navigate("Info")}>
                    <Image style={styles.icon} source={require('../assets/info.png')}/>
                </TouchableOpacity>
                    <Image style={styles.logo1} source={require('../assets/RGB_Oulu2026_gradient_horizontal.png')}/>
                <TouchableOpacity onPress={() => navigation.navigate("Profiili")}>
                    <Image style={styles.icon} source={require('../assets/profiili.png')}/>
                </TouchableOpacity>
            </View>
            <View style={styles.headingView}>
                <Text style={styles.otsikko}>{item.perinneruoka}</Text>
                <TouchableOpacity onPress={() => navigation.navigate("Suosikit")}>
                        <Image style={{width: 25, height: 22.87}} source={require('../assets/suosikit_ikoni_sininen.png')}/>
                </TouchableOpacity>
            </View>
            <ScrollView contentContainerStyle={styles.scrollviewContainer}>
                <View style={styles.contentContainer}>
                    <View style={styles.card}>
                        <View style={{ alignItems: 'center'}}>
                            <Image source={item.perinneruokakuva} style={{ height: 200, width: '100%', borderTopLeftRadius: 15, borderTopRightRadius: 15}} />
                            {/* Näytetään valitun reseptin kuvaus viittaamalla ylhäällä määritetltyyn item muuttujaan */}
                            <Text style={styles.detailsText}>{item.perinneruokakuvaus}</Text>
                                <View style={styles.cityLabel}>
                                    <Image style={styles.cityIcon} source={require('../assets/sijainti.png')}/>
                                    <Text style={styles.cityName}>{item.nimi}</Text>
                                </View>
                                <Text style={{ color: '#001E60', fontSize: 20, fontWeight: 'bold', padding: 10 }}> Ainekset ja valmistus</Text>
                                <View style={styles.cookingtimeLabel}>
                                    <Image style={{width: 20, height: 20}} source={require('../assets/valmistusaika.png')}/>
                                    <Text style={{ fontSize: 16, color: '#001E60', paddingHorizontal: 10}}>{item.valmistusaika}</Text>
                                </View>
                                <Text style={styles.detailsText}>
                                    {/* Jako aineksien välillä uuteen riviin "\n" avulla ja muokkaus map()-funktiolla. */}
                                    {item.ainekset.split('\n').map((item, key) => {
                                        return <Text key={key}>{item}{"\n"}</Text>
                                    })}
                                </Text>
                                <Text style={styles.detailsText}>
                                    {/* Jako valmistusohjeen välillä uuteen riviin "\n" avulla ja muokkaus map()-funktiolla. */}
                                    {item.valmistus.split('\n').map((item, key) => {
                                        return <Text key={key}>{item}{"\n"}</Text>
                                    })}
                                </Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
            {/* Tuodaan navigaatiopalkki ohjelman alareunaan */}
            <NavigationBar navigation={navigation}/>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: '#F6F7F1',
    },
    topView: {
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        position: 'absolute',
        backgroundColor: '#F6F7F1',
    },
    logo1: {
        width: 100,
        height: 47.29,
    },
    icon: {
        width: 30,
        height: 30,
    },
    headingView:{
        marginTop: 120,
        left: 27,
        width: '85%',
        height: 'auto',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
    },
    otsikko: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#001E60',
        paddingBottom: 10,
    },
    scrollviewContainer: {
        flexGrow: 1,
        paddingBottom: 40,
    },
    contentContainer: {
        marginTop: 10,
        marginBottom: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 10,
    },
    detailsText: {
        width: '95%',
        padding: 10,
        marginTop: 10,
        lineHeight: 22,
        fontSize: 16,
        textAlign: 'center',
        color: 'black',
    },
    cookingtimeLabel: {
        margin: 5,
        width: 'auto',
        height: 'auto',
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    card: {
        width: '95%',
        height: 'auto',
        backgroundColor: '#fff',
        borderRadius: 15,
        borderWidth: 1,
        borderColor: '#ddd',
        marginTop: 10,
        marginBottom: 10,
        paddingBottom: 20,
    },
    cityLabel: {
        margin: 15,
        padding: 12,
        width: 'auto',
        height: 'auto',
        borderRadius: 15,
        backgroundColor: '#001E60',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    cityName:{
        lineHeight: 22,
        fontSize: 16,
        color: 'white',
        paddingStart: 10,
    },
    cityIcon:{
        width: 20,
        height: 20,
    },

});
