import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NavigationBar from '../consts/NavigationBar.js';

const Profile = ({ navigation }) => {
    // Tallennetaan käyttäjän syöttämät tiedot sähköpostista ja salasanasta useStaten avulla
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errorMessage, setErrorMessage] = useState('');

    // Tarkistetaan, onko käyttäjä jo kirjautunut tililleen. Jos ei ole, jatketaan kirjautumisprosessia normaalisti eteenpäin.
    // Jos on, käyttäjä ohjataan suoraan profiilinäkymään
    useEffect(() => {
        const checkLoggedIn = async () => {
            const loggedIn = await AsyncStorage.getItem('loggedIn');
            if (loggedIn === 'true') {
                navigation.navigate("Profiilitiedot");
            }
        };
        checkLoggedIn();
    }, []);

    const handleLogin = async () => {
        try {
            // Haetaan tallennetut käyttäjän tiedot muistista.
            const userData = await AsyncStorage.getItem('userData');
            if (userData !== null) {
                // Muutetaan sähköpostiosoite ja salasana JSON-muodosta Javascript objektiksi
                const { email: savedEmail, password: savedPassword } = JSON.parse(userData);
                // Vertaillaan tallennettuja tietoja kirjautumisikkunan syötteisiin. 
                // Jos tiedot täsmäävät, asetetaan käyttäjä kirjautuneeksi sovellukseen ja ohjataan profiilinäkymään.
                if (savedEmail === email && savedPassword === password) {
                    await AsyncStorage.setItem('loggedIn', 'true');
                    navigation.navigate("Profiilitiedot");
                    // Jos tiedot eivät vastaa toisiaan, annetaan virheilmoitus.
                } else {
                    Alert.alert(
                        "Virheellinen sähköpostiosoite tai salasana",
                        "Tarkista kirjautumistiedot ja yritä uudelleen.",
                        [
                            { text: "Jatka" }
                        ]
                    );
                }
            }
        } catch (error) {
            console.error(error);
        }

    };

    // Tästä eteenpäin ohjelma noudattaa pitkälti Favourites-näytön kommentteja
    return (
        <View style={styles.container}>
            <View style={styles.topView}>
                <TouchableOpacity onPress={() => navigation.navigate("Info")}>
                    <Image style={styles.icon} source={require('../assets/info.png')} />
                </TouchableOpacity>
                <Image style={styles.logo1} source={require('../assets/RGB_Oulu2026_gradient_horizontal.png')} />
                <TouchableOpacity onPress={() => navigation.navigate("Profiili")}>
                    <Image style={styles.icon} source={require('../assets/profiili.png')} />
                </TouchableOpacity>
            </View>
            <View style={styles.content}>
                <Text style={styles.screenName}>Kirjaudu sisään</Text>
                <View style={styles.formContainer}>
                    <Text style={styles.inputLabel}>Sähköposti *</Text>
                    <TextInput style={styles.input} placeholderTextColor="#003f5c" onChangeText={(text) => setEmail(text)} />
                    <Text style={styles.inputLabel}>Salasana *</Text>
                    <TextInput secureTextEntry style={styles.input} placeholderTextColor="#003f5c" onChangeText={(text) => setPassword(text)} />
                </View>
                <TouchableOpacity>
                    {/* Käyttäjä ohjataan kyseiseen näkymään, jos hän klikkaa tekstistä. */}
                    <Text style={styles.forgot} onPress={() => navigation.navigate("Unohtuiko salasana")}>Unohtuiko salasana?</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.loginBtn} onPress={handleLogin}>
                    {/* Käyttäjä kirjautuu sovellukseen painamalla tätä nappia kutsuen handleLogin funktiota */}
                    <Text style={styles.loginText}>Kirjaudu sisään</Text>
                </TouchableOpacity>
                <Text style={styles.errorMessage}>{errorMessage}</Text>
                <TouchableOpacity style={styles.loginBtn} onPress={() => navigation.navigate("Rekisteröidy")}>
                    {/* Käyttäjä ohjataan rekisteröitymissivulle tätä nappia painamalla */}
                    <Text style={styles.loginText}>Rekisteröidy</Text>
                </TouchableOpacity>
            </View>
            <NavigationBar navigation={navigation}/>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F6F7F1',
    },
    topView: {
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        position: 'absolute',
        backgroundColor: '#F6F7F1',
    },
    logo1: {
        width: 100,
        height: 47.29,
    },
    icon: {
        width: 30,
        height: 30,
    },
    content: {
        marginTop: 120,
    },
    screenName: {
        left: 27,
        fontSize: 20,
        paddingBottom: 40,
        fontWeight: 'bold',
        color: '#001E60',
    },
    formContainer: {
        left: 27,
        marginBottom: 20,
        width: '80%',
    },
    inputLabel: {
        fontSize: 15,
        paddingBottom: 10,
        color: '#001E60',
    },
    input: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        height: 40,
        marginBottom: 20,
        paddingHorizontal: 10
    },
    forgot: {
        color: '#001E60',
        fontSize: 12,
        justifyContent: 'center',
        alignSelf: 'center',
        textDecorationLine: 'underline',
        marginBottom: 20,
    },
    loginBtn: {
        width: '40%',
        backgroundColor: '#001E60',
        borderRadius: 25,
        height: 50,
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 20,
    },
    loginText: {
        color: 'white',
        justifyContent: 'center',
        alignSelf: 'center',
        fontWeight: 'bold',
    },

});

export default Profile;