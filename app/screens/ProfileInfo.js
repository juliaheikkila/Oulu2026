import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, SafeAreaView, TouchableHighlight } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NavigationBar from '../consts/NavigationBar.js';

const ProfileInfo = ({ navigation }) => {
    // Alustetaan muuttujat useStaten avulla
    const [loggedIn, setLoggedIn] = useState(false);
    const [user, setUser] = useState(null);

    const checkLoggedInStatus = async () => {
        try {
            // Haetaan AsyncStoragesta kirjautumistilan tieto
            const loggedInStatus = await AsyncStorage.getItem('loggedIn');
            if (loggedInStatus === 'true') {
                setLoggedIn(true);
                // Haetaan AsyncStoragesta käyttäjän tiedot, jotta profiilissa näkyy käyttäjän etu- ja sukunimi.
                const userData = await AsyncStorage.getItem('userData');
                // Parsitaan JSON-objekti Javascript objektiksi 
                const user = JSON.parse(userData);
                setUser(user);
            } else {
                setLoggedIn(false);
            }
        } catch (error) {
            console.log(error);
        }
    };

    useEffect(() => {
        checkLoggedInStatus();
    }, []);

    // Tällä funktiolla suoritetaan käyttäjän uloskirjautuminen tililtä
    const handleLogout = async () => {
        try {
            // Poistetaan AsyncStoragesta loggedIn tieto
            await AsyncStorage.removeItem('loggedIn');
            // Muutetaan kirjautumisen tila falseksi
            setLoggedIn(false);
            // Asetetaan käyttäjätiedot tyhjäksi
            setUser(null);
            navigation.navigate("Profiili");
        } catch (error) {
            console.log(error);
        }
    };
    
    // Tästä eteenpäin koodi noudattaa pitkälti Favourites-näytön kommentteja
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.topView}>
                <TouchableOpacity onPress={() => navigation.navigate("Info")}>
                    <Image style={styles.icon} source={require('../assets/info.png')}/>
                </TouchableOpacity>
                    <Image style={styles.logo1} source={require('../assets/RGB_Oulu2026_gradient_horizontal.png')}/>
                <TouchableOpacity onPress={() => navigation.navigate("Profiili")}>
                    <Image style={styles.icon} source={require('../assets/profiili.png')}/>
                </TouchableOpacity>
            </View>
            <View style={styles.content}>
                <Text style={styles.screenName}>Profiili</Text>
                <View style={styles.userContainer}>
                    <Image source={require('../assets/profiilikuva.png')} style={styles.logo} />
                    {/* Sovelluksessa näytetään käyttäjän etu- ja sukunimi. */}
                    <Text style={styles.userName}>{user ? user.firstName + ' ' + user.lastName : ""}</Text>
                </View>
                <Text style={styles.headerText}>Asetukset</Text>
                {/* Navigoidaan Käyttäjäasetukset-näkymään */}
                <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("Käyttäjän asetukset")}>
                    <Image style={{width: 18, height: 18}} source={require('../assets/asetukset.png')}/>
                    <Text style={styles.buttonText}>Käyttäjän asetukset</Text>
                </TouchableOpacity>
                {/* Navigoidaan Salasanan vaihto -näkymään */}
                <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("Salasanan vaihto")}>
                    <Image style={{width: 16.8, height: 19.46}} source={require('../assets/salasana.png')}/>
                    <Text style={styles.buttonText}>Salasanan vaihto</Text>
                </TouchableOpacity>
                {/* Kutsutaan handleLogout funktiota käyttäjän uloskirjaamiseksi */}
                <TouchableOpacity style={styles.button} onPress={handleLogout}>
                    <Image style={{width: 14.22, height: 19.46}} source={require('../assets/kirjauduulos.png')}/>
                    <Text style={styles.buttonText}>Kirjaudu ulos</Text>
                </TouchableOpacity>
            </View>
            <NavigationBar navigation={navigation}/>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F6F7F1'
    },
    topView: {
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        position: 'absolute',
        backgroundColor: '#F6F7F1',
    },
    logo1: {
        width: 100,
        height: 47.29,
    },
    icon: {
        width: 30,
        height: 30,
    },
    content: {
        marginTop: 120,
    },
    screenName: {
        left: 27,
        fontSize: 20, 
        paddingBottom: 40,
        fontWeight: 'bold', 
        color: '#001E60',
    },
    userContainer: {
        backgroundColor: '#ffff',
        borderRadius: 15,
        width: '80%',
        height: '30%',
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        alignSelf: 'center',
        paddingBottom: 10,
        marginBottom: 20,
    },
    logo: {
        width: 80,
        height: 80,
        alignSelf: 'center',
    },
    userName: {
        fontSize: 20,
        color: '#001E60',
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    headerText: {
        left: 27,
        fontSize: 17, 
        paddingBottom: 20,
        fontWeight: 'bold', 
        color: '#001E60',
    },
    button: {
        left: 27,
        backgroundColor: '#ffff',
        width: '80%',
        height: 50,
        borderRadius: 15,
        marginBottom: 20,
        paddingHorizontal: 15,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    buttonText: {
        color: '#001E60',
        fontSize: 15,
        fontWeight: 'bold',
        paddingHorizontal: 10,
    },

});

export default ProfileInfo;
