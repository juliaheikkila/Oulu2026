import React, {useState}  from 'react';
import { SafeAreaView, Text, View, StyleSheet, Image, FlatList, TouchableHighlight, TouchableOpacity } from 'react-native';
import ruoat from '../consts/kaupungit.js';
import SearchBar from '../consts/SearchBar.js';
import NavigationBar from '../consts/NavigationBar.js';

export default function Recipes({ navigation }) {

    const [searchPhrase, setSearchPhrase] = useState("");
    const [clicked, setClicked] = useState(false);

    const Card = ({ resepti }) => {

        return (
                // tästä alaspäin määritellään resepti muuttujan avulla yhden reseptiobjektin tiedot, niin monta kertaa kuin flatlistissa on objekteja. 
                // tässä tapauksessa rivin 3 importilla ruoat muttujaan viitataan rivin 58 sorttauksessa, ja sortedRecipies muuttujaa käytetään puolestaan flatlistin datalähteenä rivillä 77.
                // ruoat muuttuja sisältää tässä sovelluksessa 4 eri reseptiä, joten myös neljän eri reseptin tiedot näytetään käyttäjälle.
                <TouchableHighlight
                    underlayColor={'#F6F7F1'}
                    activeOpacity={0.9}
                    onPress={() => navigation.navigate('Reseptisivu', resepti)}>
                    <View style={styles.card}>
                        <View style={{ alignItems: 'center' }}>
                            <Image source={resepti.perinneruokakuva} style={{ height: '85%', width: '100%', borderTopLeftRadius: 15, borderTopRightRadius: 15 }} />
                        </View>
                        <View style={{marginHorizontal: 15, marginTop: -20}}>
                            <View style={styles.cardTitleView}>
                                <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#001E60' }}>{resepti.perinneruoka}</Text>
                                <TouchableOpacity onPress={() => navigation.navigate("Suosikit")}>
                                        <Image style={{width: 20, height: 18.3, marginTop: 5,}} source={require('../assets/suosikit_ikoni_sininen.png')}/>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.cookingtimeLabel}>
                                    <Image style={{width: 15, height: 15}} source={require('../assets/valmistusaika.png')}/>
                                    <Text style={{fontSize: 12, color: '#001E60', paddingHorizontal: 5}}>{resepti.valmistusaika}</Text>
                            </View>
                        </View>
                    </View>
                </TouchableHighlight>
        );
    };

    const renderItem = ({ item }) => {
        // when no input, show all
        if (searchPhrase === "") {
            return <Card resepti={item} />;
        }
        // filter of the name
        if (item.perinneruoka.toUpperCase().includes(searchPhrase.toUpperCase().trim().replace(/\s/g, ""))) {
            return <Card resepti={item} />;
        }

    };
    // järjestetään reseptit aakkosjärjestykseen
    const sortedRecipes = ruoat.sort((a, b) => a.perinneruoka.localeCompare(b.perinneruoka));

    // tästä alaspäin koodi noudattaa pitkälti favourites luokan kommentteja
    return (
        <SafeAreaView style={styles.background}>
            <View style={styles.topView}>
                <TouchableOpacity onPress={() => navigation.navigate("Info")}>
                    <Image style={styles.icon} source={require('../assets/info.png')}/>
                </TouchableOpacity>
                    <Image style={styles.logo1} source={require('../assets/RGB_Oulu2026_gradient_horizontal.png')}/>
                <TouchableOpacity onPress={() => navigation.navigate("Profiili")}>
                    <Image style={styles.icon} source={require('../assets/profiili.png')}/>
                </TouchableOpacity>
            </View>
            <View style={styles.content} onStartShouldSetResponder={() => {setClicked(false);}}>
                <Text style={styles.screenName}>Reseptit</Text>
                <SearchBar searchPhrase={searchPhrase} setSearchPhrase={setSearchPhrase} clicked={clicked} setClicked={setClicked}></SearchBar>
                {/* määritellään flatlist, jonka avulla reseptit näytetään käyttäjälle. Flatlistille annetaan tietolähteeksi aakkosjärjestykseen laitetut reseptit. */}
                {/* id toimii jokaisen kohteen yksilöivänä tunnisteena */}
                <FlatList data={sortedRecipes} renderItem={renderItem} keyExtractor={(item) => item.id}></FlatList>
            </View>
            {/* tuodaan navigaatiopalkki näkymän alareunaan */}
            <NavigationBar navigation={navigation}/>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: '#F6F7F1',
    },
    topView: {
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        position: 'absolute',
        backgroundColor: '#F6F7F1',
    },
    logo1: {
        width: 100,
        height: 47.29,
    },
    icon: {
        width: 30,
        height: 30,
    },
    content: {
        flex: 1,
        marginTop: 120,
        marginBottom: 60,
    },
    screenName: {
        left: 27, 
        fontSize: 20, 
        fontWeight: 'bold', 
        color: '#001E60',
        marginBottom: 10,
    },
    card: {
        height: 220,
        marginHorizontal: 20,
        marginBottom: 10,
        marginTop: 10,
        borderRadius: 15,
        elevation: 13,
        backgroundColor: '#ffff',
    },
    cardTitleView:{
        height: 'auto',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
    },
    cookingtimeLabel: {
        marginTop: 5,
        marginBottom: 5,
        width: 'auto',
        height: 'auto',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignContent: 'center',
    },


});
