import React from 'react';
import { SafeAreaView, StyleSheet, View, Text, Image, ScrollView, Linking, TouchableOpacity, TouchableHighlight } from 'react-native';
import NavigationBar from '../consts/NavigationBar.js';

const TownDetails = ({ navigation, route }) => {
    // sijoitetaan item muuttujaan navigointiparametrit, jotta käyttäjälle voidaan näyttää edellisessä näkymässä klikatun kaupungin tarkemmat tiedot
    const item = route.params;
    // tästä alaspäin koodi noudattaa pitkälti favourites luokan kommentteja
    return (
        <SafeAreaView style={styles.background}>
            <View style={styles.topView}>
                <TouchableOpacity onPress={() => navigation.navigate("Info")}>
                    <Image style={styles.icon} source={require('../assets/info.png')}/>
                </TouchableOpacity>
                    <Image style={styles.logo1} source={require('../assets/RGB_Oulu2026_gradient_horizontal.png')}/>
                <TouchableOpacity onPress={() => navigation.navigate("Profiili")}>
                    <Image style={styles.icon} source={require('../assets/profiili.png')}/>
                </TouchableOpacity>
            </View>
            <ScrollView style={styles.scrollviewContainer}>
                <View style={styles.contentContainer}>
                {/* haetaan paikkakunnan nimi itemistä */}
                <Text style={{fontSize: 20, fontWeight: 'bold', color: '#001E60', paddingLeft: 20, paddingBottom: 20}}>{item.nimi}</Text>
                    {/* haetaan paikkakunnan kuvaus */}
                    <Text style={styles.detailsText}>{item.kuvaus}</Text>
                    <Text style={styles.link}
                    // haetaan paikkakunnan nettisivun linkki
                        onPress={() => Linking.openURL(item.linkki)} >
                        {'Kaupungin nettisivut'}
                    </Text>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#001E60', textAlign: 'center', paddingBottom: 20, }}>Kaupungin perinneruokareseptejä</Text>
                        <TouchableHighlight
                        underlayColor={'#F6F7F1'}
                        activeOpacity={0.9}
                        onPress={() => navigation.navigate('Reseptisivu', item)}>
                        <View style={styles.card}>
                            <View style={{ alignItems: 'center' }}>
                                {/* haetaan kyseisen kaupungin perinneruokakuva  */}
                                <Image source={item.perinneruokakuva} style={{ height: '85%', width: '100%', borderTopLeftRadius: 15, borderTopRightRadius: 15 }} />
                            </View>
                            <View style={{marginHorizontal: 15, marginTop: -20}}>
                                <View style={styles.headingView}>
                                    {/* haetaan kaupungin perinneruoan nimi */}
                                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#001E60' }}>{item.perinneruoka}</Text>
                                    <TouchableOpacity onPress={() => navigation.navigate("Suosikit")}>
                                            <Image style={{width: 20, height: 18.3, marginTop: 5}} source={require('../assets/suosikit_ikoni_sininen.png')}/>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.cookingtimeLabel}>
                                        <Image style={{width: 15, height: 15}} source={require('../assets/valmistusaika.png')}/>
                                        {/* haetaan kaupungin perinneruoan valmistusaika */}
                                        <Text style={{fontSize: 12, color: '#001E60', paddingHorizontal: 5}}>{item.valmistusaika}</Text>
                                </View>
                            </View>
                        </View>
                        </TouchableHighlight>
                </View>
            </ScrollView>
            {/* tuodaan navigointipalkki näkymän alareunaan */}
            <NavigationBar navigation={navigation}/>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: '#F6F7F1',
    },
    topView: {
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        position: 'absolute',
        backgroundColor: '#F6F7F1',
    },
    logo1: {
        width: 100,
        height: 47.29,
    },
    icon: {
        width: 30,
        height: 30,
    },
    scrollviewContainer: {
        marginTop: 120,
    },
    contentContainer: {
        flex: 1,
        flexDirection: 'column',
        marginBottom: 300,
        paddingHorizontal: 10,
    },
    detailsText: {
        marginTop: 10,
        lineHeight: 22,
        fontSize: 17,
        paddingHorizontal: 20,
        paddingBottom: 20,
        color: 'black',
        textAlign: 'center',
    },
    link: {
        marginTop: 10,
        lineHeight: 22,
        fontSize: 17,
        paddingHorizontal: 20,
        paddingBottom: 30,
        color: '#001E60',
        textAlign: 'center',
        textDecorationLine: 'underline',
    },
    card: {
        height: 220,
        marginHorizontal: 10,
        marginBottom: 10,
        marginTop: 10,
        borderRadius: 15,
        elevation: 13,
        backgroundColor: '#ffff',
    },
    headingView:{
        height: 'auto',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
    },
    cookingtimeLabel: {
        marginTop: 5,
        marginBottom: 5,
        width: 'auto',
        height: 'auto',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignContent: 'center',
    },

});

export default TownDetails;
