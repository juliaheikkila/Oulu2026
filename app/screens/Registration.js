import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NavigationBar from '../consts/NavigationBar.js';

const Registration = ({ navigation }) => {

    // alustetaan tilat käyttäjän tietojen tallentamista varten
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [errors, setErrors] = useState({});
  
    const handleRegistration = () => {
      const newErrors = {};
      // jos käyttäjän tietoja puuttuu, annetaan virheilmoitus
      if (!firstName || !lastName || !email || !password || !confirmPassword) {
        Alert.alert(
          "Pakollisia tietoja puuttuu",
          "Ole hyvä ja täytä kaikki puuttuvat tiedot, jotta voit luoda tilin.",
          [{ text: "Jatka" }]
        );
      }
      // jos salasanat eivät täsmää, annetaan virheilmoitus 
      if (password != confirmPassword) {
        Alert.alert(
          "Salasanat eivät täsmää",
          "Tarkista syöttämäsi salasanat ja yritä uudelleen.",
          [{ text: "Jatka" }]
        );
      }
  
      if (!firstName.trim()) {
        newErrors.firstName = true;
      }
      if (!lastName.trim()) {
        newErrors.lastName = true;
      }
      if (!email.trim()) {
        newErrors.email = true;
      }
      if (!password.trim()) {
        newErrors.password = true;
      }
      if (!confirmPassword.trim()) {
        newErrors.confirmPassword = true;
      }
      if (password != confirmPassword) { 
          newErrors.password = true;
          newErrors.confirmPassword = true;
      }
      if (Object.keys(newErrors).length > 0) {
        setErrors(newErrors);
        return;
      }
  
  
      // Tässä tallennetaan tiedot Reactin tilaan.
      const userData = { firstName, lastName, email, password };
      console.log("Tallennettu käyttäjän data:", userData);
  
      AsyncStorage.setItem("userData", JSON.stringify(userData))
        .then(() => {
          console.log("Käyttäjän tiedot tallennettu onnistuneesti.");
        })
        .catch((error) => {
          console.log("Virhe tallennettaessa käyttäjän tietoja:", error);
        });
  
      navigation.navigate("Profiili");
    };
  
    const handleError = (error, input) => {
      setErrors((prevState) => ({ ...prevState, [input]: error }));
    };
  
    return (
      <View style={styles.container}>
        <View style={styles.topView}>
          <TouchableOpacity onPress={() => navigation.navigate("Info")}>
            <Image style={styles.icon} source={require("../assets/info.png")} />
          </TouchableOpacity>
          <Image
            style={styles.logo1}
            source={require("../assets/RGB_Oulu2026_gradient_horizontal.png")}
          />
          <TouchableOpacity onPress={() => navigation.navigate("Profiili")}>
            <Image
              style={styles.icon}
              source={require("../assets/profiili.png")}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.content}>
          {/* määritellään tekstikentät käyttäjän tietojen syöttämiselle */}
          <Text style={styles.screenName}>Rekisteröidy</Text>
          <View style={styles.formContainer}>
            <Text style={styles.inputLabel}>Etunimi *</Text>
            <TextInput
              style={[styles.input, errors.firstName && styles.inputError]}
              onChangeText={setFirstName}
              onFocus={() => handleError(null, "firstName")}
            />
  
            <Text style={styles.inputLabel}>Sukunimi *</Text>
            <TextInput
              style={[styles.input, errors.lastName && styles.inputError]}
              onChangeText={setLastName}
              onFocus={() => handleError(null, "lastName")}
            />
  
            <Text style={styles.inputLabel}>Sähköpostiosoite *</Text>
            <TextInput
              style={[styles.input, errors.email && styles.inputError]}
              onChangeText={setEmail}
              onFocus={() => handleError(null, "email")}
            />
  
            <Text style={styles.inputLabel}>Salasana *</Text>
            <TextInput
              style={[styles.input, errors.password && styles.inputError]}
              secureTextEntry={true}
              onChangeText={setPassword}
              onFocus={() => handleError(null, "password")}
            />
  
            <Text style={styles.inputLabel}>Vahvista salasana *</Text>
            <TextInput
              style={[styles.input, errors.confirmPassword && styles.inputError]}
              secureTextEntry={true}
              onChangeText={setConfirmPassword}
              onFocus={() => handleError(null, "confirmPassword")}
            />
  
            <TouchableOpacity style={styles.button} onPress={handleRegistration}>
              <Text style={styles.buttonText}>Luo tili</Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* Tuodaan navigointipalkki sovelluksen alapalkkiin */}
        <NavigationBar navigation={navigation} />
      </View>
    );
  };
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#F6F7F1",
    },
    topView: {
      width: "100%",
      height: 100,
      flexDirection: "row",
      justifyContent: "space-around",
      alignItems: "center",
      position: "absolute",
      backgroundColor: "#F6F7F1",
    },
    logo1: {
      width: 100,
      height: 47.29,
    },
    icon: {
      width: 30,
      height: 30,
    },
    createAccount: {
      color: "blue",
      fontWeight: "bold",
      fontSize: 18,
      textAlign: "left",
      alignSelf: "stretch",
      marginLeft: 0,
      marginTop: 10,
      top: -10,
    },
    content: {
      marginTop: 120,
    },
    screenName: {
      left: 27,
      fontSize: 20,
      paddingBottom: 40,
      fontWeight: "bold",
      color: "#001E60",
    },
    formContainer: {
      left: 27,
      marginBottom: 20,
      width: "80%",
    },
    inputLabel: {
      fontSize: 15,
      paddingBottom: 5,
      color: "#001E60",
    },
    input: {
      backgroundColor: "#FFFFFF",
      borderRadius: 10,
      height: 40,
      marginBottom: 10,
      paddingHorizontal: 10,
    },
    button: {
      width: "40%",
      backgroundColor: "#001E60",
      borderRadius: 25,
      height: 50,
      alignSelf: "center",
      justifyContent: "center",
      marginTop: 15,
    },
    buttonText: {
      color: "#FFFFFF",
      fontSize: 16,
      fontWeight: "bold",
      textAlign: "center",
    },
    inputError: {
      backgroundColor: "pink",
    },
  });
  
  export default Registration;
  