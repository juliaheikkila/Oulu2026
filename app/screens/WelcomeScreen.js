import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';

export default function WelcomeScreen({ navigation }) {
    return (
        // Näkymässä toivotetaan käyttäjä tervetulleeksi Oulu2026 kartan sekä logojen kera.
        // Tästä eteenpäin koodi noudattaa pitkälti Favourites-näytön kommentteja. 
        <SafeAreaView style={styles.background}>
            <View style={styles.content}>
                <Text style={styles.textStyle}>Tervetuloa tutustumaan Oulu2026-alueen ruokakulttuuriin!</Text>
                    <Image style={styles.kartta} source={require('../assets/kartta.jpg')} />
                    <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("Koti")}>
                        <Text style={styles.buttonText}>Aloita</Text>
                    </TouchableOpacity>
                <View style={styles.logoView}>
                    <Image style={styles.logo2} source={require('../assets/Rotisseurs_logo.png')} />
                    <Image style={styles.logo1} source={require('../assets/RGB_Oulu2026_gradient_horizontal.png')} />
                </View>
            </View> 
        </SafeAreaView>
    );
}
const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: '#F6F7F1',
    },
    content: {
        marginTop: 100,
    },
    textStyle: {
        fontSize: 20, 
        color: '#001E60', 
        textAlign: 'center',
    },
    kartta: {
        width: "100%",
        height: 350,
        marginTop: 20,
        marginBottom: 10,
    },
    button: {
        width: '30%',
        backgroundColor: '#001E60',
        borderRadius: 15,
        height: 50,
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 30,
        marginBottom: 10,
    },
    buttonText: {
        fontSize: 15,
        color: 'white',
        justifyContent: 'center',
        alignSelf: 'center',
        fontWeight: 'bold',
    },
    logoView: {
        width: '100%',
        height: 120,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    logo1: {
        width: 129,
        height: 61,
    },
    logo2: {
        width: 76,
        height: 76,
    },
});
