import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NavigationBar from '../consts/NavigationBar.js';

const ProfileSettings = ({ navigation }) => {
    // Luodaan tilamuuttujat käyttäjän tiedoille
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [errors, setErrors] = useState({});
  
    useEffect(() => {
       // Haetaan käyttäjän tiedot AsyncStoragesta. Jos dataa löytyy, se parsitaan JSON-objektista Javascript objektiksi.
      AsyncStorage.getItem("userData").then((userData) => {
        if (userData !== null) {
          const { firstName, lastName, email, password } = JSON.parse(userData);
          // Tiedot asetetaan, jotta käyttäjälle voidaan näyttää tämänhetkiset profiiliin tallenetut tiedot.
          setFirstName(firstName);
          setLastName(lastName);
          setEmail(email);
          setPassword(password);
        }
      });
    }, []);
  
    const handleChange = async () => {
      // Tarkastetaan, että pakolliset tiedot ovat täytetty. Virhetilanteessa annetaan käyttäjälle ilmoitus.
      const newErrors = {};
      if (!firstName || !lastName || !email) {
        Alert.alert(
          "Pakollisia tietoja puuttuu",
          "Ole hyvä ja täytä kaikki pakolliset kentät, jotta voit tallentaa tietosi.",
          [{ text: "Jatka" }]
        );
      }
  
      if (!firstName.trim()) {
        newErrors.firstName = true;
      }
      if (!lastName.trim()) {
        newErrors.lastName = true;
      }
      if (!email.trim()) {
        newErrors.email = true;
      }
      if (Object.keys(newErrors).length > 0) {
        setErrors(newErrors);
        return;
      }
  
      // Käyttäjän syöttämät uudet tai muuttamattomat tiedot tallennetaan takaisin AsyncStorageen.
      const userData = { firstName, lastName, email, password: password };
      console.log("Tallennettu käyttäjän data:", userData);
      // userData objekti muutetaan takaisin JSON-objektiksi tietojen tallentamista varten.
      AsyncStorage.setItem("userData", JSON.stringify(userData))
        .then(() => {
          console.log("Käyttäjän tiedot tallennettu onnistuneesti.");
        })
        .catch((error) => {
          console.log("Virhe tallennettaessa käyttäjän tietoja:", error);
        });
  
      navigation.navigate("Profiilitiedot");
    };
  
    const handleError = (error, input) => {
      setErrors((prevState) => ({ ...prevState, [input]: error }));
    };
  
    // Tästä eteenpäin koodi noudattaa pitkälti Favourites-näytön kommentteja
    return (
      <View style={styles.container}>
        <View style={styles.topView}>
          <TouchableOpacity onPress={() => navigation.navigate("Info")}>
            <Image style={styles.icon} source={require("../assets/info.png")} />
          </TouchableOpacity>
          <Image
            style={styles.logo1}
            source={require("../assets/RGB_Oulu2026_gradient_horizontal.png")}
          />
          <TouchableOpacity onPress={() => navigation.navigate("Profiili")}>
            <Image
              style={styles.icon}
              source={require("../assets/profiili.png")}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.content}>
          <Text style={styles.screenName}>Käyttäjän asetukset</Text>
          <Text style={styles.guideText}>
            Voit muuttaa käyttäjätiliisi liittyviä tietoja alla olevista kentistä.
          </Text>
          <View style={styles.formContainer}>
            <Text style={styles.settingLabel}>Etunimi *</Text>
            <TextInput
              style={[styles.input, errors.firstName && styles.inputError]}
              onChangeText={setFirstName}
              value={firstName}
              onFocus={() => handleError(null, "firstName")}
            />
  
            <Text style={styles.settingLabel}>Sukunimi *</Text>
            <TextInput
              style={[styles.input, errors.lastName && styles.inputError]}
              onChangeText={setLastName}
              value={lastName}
              onFocus={() => handleError(null, "lastName")}
            />
  
            <Text style={styles.settingLabel}>Sähköpostiosoite *</Text>
            <TextInput
              style={[styles.input, errors.email && styles.inputError]}
              onChangeText={setEmail}
              value={email}
              onFocus={() => handleError(null, "email")}
            />
  
            <TouchableOpacity style={styles.button} onPress={handleChange}>
              <Text style={styles.buttonText}>Vahvista muutokset</Text>
            </TouchableOpacity>
          </View>
        </View>
        <NavigationBar navigation={navigation} />
      </View>
    );
  };
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#F6F7F1",
    },
    topView: {
      width: "100%",
      height: 100,
      flexDirection: "row",
      justifyContent: "space-around",
      alignItems: "center",
      position: "absolute",
      backgroundColor: "#F6F7F1",
    },
    logo1: {
      width: 100,
      height: 47.29,
    },
    icon: {
      width: 30,
      height: 30,
    },
    content: {
      marginTop: 120,
    },
    screenName: {
      left: 27,
      fontSize: 20,
      paddingBottom: 40,
      fontWeight: "bold",
      color: "#001E60",
    },
    guideText: {
      left: 27,
      fontSize: 15,
      marginBottom: 30,
      width: "80%",
    },
    formContainer: {
      left: 27,
      marginBottom: 20,
      width: "80%",
    },
    settingLabel: {
      fontSize: 15,
      paddingBottom: 10,
      color: "#001E60",
    },
    input: {
      backgroundColor: "#FFFFFF",
      borderRadius: 10,
      height: 40,
      marginBottom: 20,
      paddingHorizontal: 10,
    },
    button: {
      width: "60%",
      backgroundColor: "#001E60",
      borderRadius: 25,
      height: 50,
      alignSelf: "center",
      justifyContent: "center",
      marginTop: 20,
    },
    buttonText: {
      color: "#FFFFFF",
      fontSize: 16,
      fontWeight: "bold",
      textAlign: "center",
    },
    inputError: {
      backgroundColor: "pink",
    },
  });
  
  export default ProfileSettings;
  