import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as MailComposer from 'expo-mail-composer';
import NavigationBar from '../consts/NavigationBar.js';

const ForgotPassword = ({ navigation }) => {
    // määritetään useStaten avulla tila, jotta käyttäjän sähköpostiosoitteeseen voidaan lähettää vahvistuskoodi
    const [email, setEmail] = useState('');

    const handleSendCode = async () => {
        // generoi satunnaisen 6-numeroisen koodin
        const code = Math.floor(100000 + Math.random() * 900000).toString();

        // tallenna koodi AsyncStorageyn
        await AsyncStorage.setItem('verificationCode', code);

        // lähetä koodi käyttäjän sähköpostiin
        MailComposer.composeAsync({
            recipients: [email],
            subject: 'Vahvistuskoodi salasanan vaihtoa varten',
            body: `Vahvistuskoodi salasanan vaihtoa varten: ${code}`
        });

        navigation.navigate("Varmistuskoodi");
    }

    // Tästä eteenpäin ohjelma noudattaa pitkälti Favourites-näytön kommentteja
    return (
        <View style={styles.container}>
            <View style={styles.topView}>
                <TouchableOpacity onPress={() => navigation.navigate("Info")}>
                    <Image style={styles.icon} source={require('../assets/info.png')}/>
                </TouchableOpacity>
                    <Image style={styles.logo1} source={require('../assets/RGB_Oulu2026_gradient_horizontal.png')}/>
                <TouchableOpacity onPress={() => navigation.navigate("Profiili")}>
                    <Image style={styles.icon} source={require('../assets/profiili.png')}/>
                </TouchableOpacity>
            </View>
            <View style={styles.content}>
                <Text style={styles.screenName}>Unohtuiko salasana?</Text>
                <Text style={styles.guideText}>Syötä käyttäjätiliisi linkitetty sähköpostiosoite, niin lähetämme varmistuskoodin salasanan vaihtoa varten kyseiseen osoitteeseen.</Text>
                    <Text style={styles.inputLabel}>Sähköpostiosoite *</Text>
                     <TextInput style={styles.input} value={email} onChangeText={setEmail}/>
                        <TouchableOpacity style={styles.button} onPress={handleSendCode}>
                            <Text style={styles.buttonText}>Lähetä koodi</Text>
                        </TouchableOpacity>
            </View>
            <NavigationBar navigation={navigation}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F6F7F1',
    },
    topView: {
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        position: 'absolute',
        backgroundColor: '#F6F7F1',
    },
    logo1: {
        width: 100,
        height: 47.29,
    },
    icon: {
        width: 30,
        height: 30,
    },
    screenName: {
        left: 27,
        fontSize: 20, 
        paddingBottom: 40,
        fontWeight: 'bold', 
        color: '#001E60',
    },
    content: {
        marginTop: 120,
    },
    guideText: {
        left: 27,
        fontSize: 15, 
        marginBottom: 30,
        width: '80%',
    },
    inputLabel:{
        left: 27,
        fontSize: 15, 
        paddingBottom: 10,
        color: '#001E60',
    },
    input: {
        left: 27,
        width: '80%',
        backgroundColor: '#fff',
        borderRadius: 15,
        height: 50,
        marginBottom: 20,
        justifyContent: 'center',
        padding: 10,
    },
    button: {
        width: '40%',
        backgroundColor: '#001E60',
        borderRadius: 25,
        height: 50,
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 20,
    },
    buttonText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center'
    },

});

export default ForgotPassword;
