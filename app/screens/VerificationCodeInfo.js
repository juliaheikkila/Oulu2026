import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, FlatList } from 'react-native';
import NavigationBar from '../consts/NavigationBar.js';

const VerificationCode = ({ navigation }) => {
  
    // Tästä eteenpäin ohjelma noudattaa pitkälti Favourites-näytön kommentteja
    return (
        <View style={styles.container}>
            <View style={styles.topView}>
                <TouchableOpacity onPress={() => navigation.navigate("Info")}>
                    <Image style={styles.icon} source={require('../assets/info.png')}/>
                </TouchableOpacity>
                    <Image style={styles.logo1} source={require('../assets/RGB_Oulu2026_gradient_horizontal.png')}/>
                <TouchableOpacity onPress={() => navigation.navigate("Profiili")}>
                    <Image style={styles.icon} source={require('../assets/profiili.png')}/>
                </TouchableOpacity>
            </View>
            <View style={styles.content}>
                <Text style={styles.screenName}>Etkö saanut koodia tai koodi on väärä?</Text>
                <Text style={styles.guideText}>Jos et saanut varmistuskoodia tai koodi on väärä, yritä seuraavia toimenpiteitä:</Text>
                        {/* Luodaan luettelomerkitty lista, jossa käytetty Unicode-merkkiä.  */}
                        <FlatList
                        data={[
                            { key: 'Odota hetki, sähköpostin lähettämisessä voi kestää joitakin minuutteja.' },
                            { key: 'Tarkista roskaposti.' },
                            { key: 'Tarkista sähköpostiosoite, jonka syötit edellisessä näkymässä.' },
                            { key: 'Tarkista varmistuskoodi, joka lähetettiin syöttämääsi sähköpostiosoitteeseen.' },
                        ]}
                        renderItem={({ item }) => {
                            return (
                                <Text style={styles.infoText}>{`\u2022 ${item.key}`}</Text>
                            );
                        }}
                    />
            </View>
            <Text style={styles.guideText}>Mikäli edellä olevat toimenpiteet eivät auta, voit siirtyä lähettämään koodin uudestaan alla olevasta linkistä:</Text>
                {/* Tekstiä painamalla navigoidaan takaisin Unohtuiko salasana -näyttöön. */}
                <TouchableOpacity>
                    <Text style={styles.forgot} onPress={() => navigation.navigate("Unohtuiko salasana")}>Lähetä koodi uudestaan</Text>
                </TouchableOpacity>
            <NavigationBar navigation={navigation}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F6F7F1',
    },
    topView: {
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        position: 'absolute',
        backgroundColor: '#F6F7F1',
    },
    logo1: {
        width: 100,
        height: 47.29,
    },
    icon: {
        width: 30,
        height: 30,
    },
    content: {
        marginTop: 120,
    },
    screenName: {
        left: 27,
        fontSize: 20, 
        paddingBottom: 40,
        width: '90%',
        fontWeight: 'bold', 
        color: '#001E60',
    },
    guideText: {
        left: 27,
        fontSize: 15, 
        marginBottom: 30,
        width: '90%',
        fontWeight: '500',
    },
    infoText: {
        left: 27,
        fontSize: 15, 
        marginBottom: 30,
        width: '80%',
    },
    forgot: {
        color: '#001E60',
        fontSize: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        textDecorationLine: 'underline',
    },

});

export default VerificationCode;
