import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NavigationBar from '../consts/NavigationBar.js';

const VerificationCode = ({ navigation }) => {
    const [code, setCode] = useState('');

    const handleVerifyCode = async () => {
        // Hae tallennettu vahvistuskoodi AsyncStoragesta
        const storedCode = await AsyncStorage.getItem('verificationCode');
        // jos käyttäjän syöttämä koodi on sama kuin arvottu koodi, siirrytään seuraavaan näkymään, jossa käyttäjä voi vaihtaa salasanansa
        console.log('Tallennettu vahvistuskoodi:', storedCode);
        if (code === storedCode) {
            // siirry seuraavaan näkymään, jossa käyttäjä voi vaihtaa salasanansa
            navigation.navigate("Salasanan vaihto");
            console.log("Oikein");
            // annetaan virheilmoitus virheellisestä koodista
        } else {
            Alert.alert('Väärä varmistuskoodi', "Syöttämäsi varmistuskoodi on väärä. Tarkista syöttämäsi koodi ja yritä uudelleen.", [{ text: "Jatka" }]);
        }
    }
    // tästä eteenpäin koodi noudattaa pitkälti favourites luokan kommentteja
    return (
        <View style={styles.container}>
            <View style={styles.topView}>
                <TouchableOpacity onPress={() => navigation.navigate("Info")}>
                    <Image style={styles.icon} source={require('../assets/info.png')}/>
                </TouchableOpacity>
                    <Image style={styles.logo1} source={require('../assets/RGB_Oulu2026_gradient_horizontal.png')}/>
                <TouchableOpacity onPress={() => navigation.navigate("Profiili")}>
                    <Image style={styles.icon} source={require('../assets/profiili.png')}/>
                </TouchableOpacity>
            </View>
            <View style={styles.content}>
                 {/* tästä alaspäin määritellään tekstikentät ja ohjeistukset koodin vahvistamista varten */}
                <Text style={styles.screenName}>Varmistaminen</Text>
                <Text style={styles.guideText}>Syötä saamasi varmistuskoodi alla olevaan kenttään ja jatka vaihtamaan salasana.</Text>
                    <Text style={styles.inputLabel}>Varmistuskoodi *</Text>
                    <TextInput style={styles.input} value={code} onChangeText={setCode}/>
                    <TouchableOpacity>
                        <Text style={styles.forgot} onPress={() => navigation.navigate("Varmistuskoodi info")}>Etkö saanut koodia tai koodi on väärä?</Text>
                    </TouchableOpacity>
                    {/* varmista nappia painettaessa kutsutaan handleVerifyCode funktiota */}
                <TouchableOpacity style={styles.button} onPress={handleVerifyCode}>
                    <Text style={styles.buttonText}>Varmista</Text>
                </TouchableOpacity>
            </View>
            {/* tuodaan navigaatiopalkki sovelluksen alareunaan */}
            <NavigationBar navigation={navigation}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F6F7F1',
    },
    topView: {
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        position: 'absolute',
        backgroundColor: '#F6F7F1',
    },
    logo1: {
        width: 100,
        height: 47.29,
    },
    icon: {
        width: 30,
        height: 30,
    },
    content: {
        marginTop: 120,
    },
    screenName: {
        left: 27,
        fontSize: 20, 
        paddingBottom: 40,
        fontWeight: 'bold', 
        color: '#001E60',
    },
    guideText: {
        left: 27,
        fontSize: 15, 
        marginBottom: 30,
        width: '80%',
    },
    inputLabel:{
        left: 27,
        fontSize: 15, 
        paddingBottom: 10,
        color: '#001E60',
    },
    input: {
        left: 27,
        width: '80%',
        backgroundColor: '#fff',
        borderRadius: 15,
        height: 50,
        marginBottom: 20,
        justifyContent: 'center',
        padding: 10,
    },
    forgot: {
        color: '#001E60',
        fontSize: 12,
        justifyContent: 'center',
        alignSelf: 'center',
        textDecorationLine: 'underline',
    },
    button: {
        width: '40%',
        backgroundColor: '#001E60',
        borderRadius: 25,
        height: 50,
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 20,
    },
    buttonText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center'
    },

});

export default VerificationCode;
