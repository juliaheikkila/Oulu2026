import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NavigationBar from '../consts/NavigationBar.js';

const PasswordChange = ({ navigation }) => {
    // Asetetaan useStatea hyödyntämällä uudet arvot salasanan vaihtamiseksi
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [errors, setErrors] = useState({});

    const handlePassword = async () => {
        try {
            // Haetaan AsyncStoragesta käyttäjän tiedot
            const userData = await AsyncStorage.getItem('userData');
            // Tarkistetaan, että käyttäjätiedot löytyvät
            if (userData !== null) {
                // Parsitaan käyttäjätiedot JSON-muodosta Javascript objektiksi
                const parsedUserData = JSON.parse(userData);
                // Tarkastetaan, että salasana-kentät on täydennetty ja annetaan virheestä ilmoitus
                if (password === '' || confirmPassword === '') {
                    Alert.alert("Pakollisia tietoja puuttuu", "Ole hyvä ja täytä kaikki pakolliset kentät, jotta voit vaihtaa salasanan.", [{ text: "Jatka" }]);
                  // Tarkastetaan, että salasana on eri kuin aiempi ja annetaan virheestä ilmoitus
                } else if (password === parsedUserData.password) {
                    Alert.alert("Uusi salasana ei voi olla sama kuin vanha salasana", "Salasanan tulee olla eri kuin aiemmin käyttämäsi salasanat.", [{ text: "Jatka" }]);
                } else if (password === confirmPassword) {
                    // Luodaan päivitetyt käyttäjätiedot
                    const updatedUserData = { ...parsedUserData, password: password };
                    // Tallennetaan päivitetyt tiedot AsyncStorageen
                    await AsyncStorage.setItem('userData', JSON.stringify(updatedUserData));
                    Alert.alert("Salasana vaihdettu!", "Salasana on vaihdettu onnistuneesti.", [
                        { text: "Jatka" }
                    ]);
                    navigation.navigate('Profiili');
                    // Annetaan käyttäjälle ilmoitus jos uusi ja vahvistettu salasana eivät täsmää
                } else {
                    Alert.alert('Salasanat eivät täsmää', "Tarkista syöttämäsi salasanat ja yritä uudelleen.", [{ text: "Jatka" }]);
                }
            }
        } catch (error) {
            console.error(error);
        }
        const newErrors = {};

        if (!confirmPassword.trim()) {
          newErrors.confirmPassword = true;
        }
        
        if (password === '' || confirmPassword === '') {
          newErrors.password = true;
          newErrors.confirmPassword = true;
        }

        if (password != confirmPassword) {
            newErrors.password = true;
            newErrors.confirmPassword = true;
        }
        if (Object.keys(newErrors).length > 0) {
          setErrors(newErrors);
          return;
        }
      };
    
      const handleError = (error, input) => {
        setErrors((prevState) => ({ ...prevState, [input]: error }));
    };

    // Tästä eteenpäin koodi noudattaa pitkälti Favourites-näytön kommentteja lukuunottamatta tekstikenttiä, joilla luodaan paikat salasanoille yms.
    return (
        <View style={styles.container}>
          <View style={styles.topView}>
            <TouchableOpacity onPress={() => navigation.navigate("Info")}>
              <Image style={styles.icon} source={require("../assets/info.png")} />
            </TouchableOpacity>
            <Image
              style={styles.logo1}
              source={require("../assets/RGB_Oulu2026_gradient_horizontal.png")}
            />
            <TouchableOpacity onPress={() => navigation.navigate("Profiili")}>
              <Image
                style={styles.icon}
                source={require("../assets/profiili.png")}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.content}>
            <Text style={styles.screenName}>Salasanan vaihto</Text>
            <Text style={styles.guideText}>
              Luo uusi salasana käyttäjätilillesi. Salasanan tulee olla eri kuin
              aiemmin käyttämäsi salasanat.
            </Text>
            <View style={styles.formContainer}>
              <Text style={styles.settingLabel}>Uusi salasana *</Text>
              <TextInput
                style={[styles.input, errors.password && styles.inputError]}
                value={password}
                onChangeText={setPassword}
                secureTextEntry={true}
                onFocus={() => handleError(null, "password")}
              />
    
              <Text style={styles.settingLabel}>Vahvista uusi salasana *</Text>
              <TextInput
                style={[styles.input, errors.confirmPassword && styles.inputError]}
                value={confirmPassword}
                onChangeText={setConfirmPassword}
                secureTextEntry={true}
                onFocus={() => handleError(null, "confirmPassword")}
              />
    
              <TouchableOpacity style={styles.button} onPress={handlePassword}>
                <Text style={styles.buttonText}>Vaihda salasana</Text>
              </TouchableOpacity>
            </View>
          </View>
          <NavigationBar navigation={navigation} />
        </View>
      );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F6F7F1',
    },
    topView: {
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        position: 'absolute',
        backgroundColor: '#F6F7F1',
    },
    logo1: {
        width: 100,
        height: 47.29,
    },
    icon: {
        width: 30,
        height: 30,
    },
    content: {
        marginTop: 120,
    },
    screenName: {
        left: 27,
        fontSize: 20,
        paddingBottom: 40,
        fontWeight: 'bold',
        color: '#001E60',
    },
    guideText: {
        left: 27,
        fontSize: 15,
        marginBottom: 30,
        width: '80%',
    },
    formContainer: {
        left: 27,
        marginBottom: 20,
        width: '80%',
    },
    settingLabel: {
        fontSize: 15,
        paddingBottom: 10,
        color: '#001E60',
    },
    input: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        height: 40,
        marginBottom: 20,
        paddingHorizontal: 10
    },
    button: {
        width: '50%',
        backgroundColor: '#001E60',
        borderRadius: 25,
        height: 50,
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 20,
    },
    buttonText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    inputError: {
        backgroundColor: "pink",
      },
});

export default PasswordChange;
