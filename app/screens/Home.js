import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import NavigationBar from '../consts/NavigationBar.js';

export default function Home({ navigation }) {
    
    // Tästä eteenpäin ohjelma noudattaa pitkälti Favourites-näytön kommentteja
    return (
        
        <SafeAreaView style={styles.background}>
            <View style={styles.topView}>
                <TouchableOpacity onPress={() => navigation.navigate("Info")}>
                    <Image style={styles.icon} source={require('../assets/info.png')}/>
                </TouchableOpacity>
                    <Image style={styles.logo1} source={require('../assets/RGB_Oulu2026_gradient_horizontal.png')}/>
                <TouchableOpacity onPress={() => navigation.navigate("Profiili")}>
                    <Image style={styles.icon} source={require('../assets/profiili.png')}/>
                </TouchableOpacity>
            </View>
            <View style={styles.content}>
                <Text style={styles.headertextStyle}> Ota pohjoinen makumaailma haltuusi</Text>
                <Image style={styles.kartta} source={require('../assets/kartta.jpg')}/>
                <Text style={styles.bodytextStyle}>Selaa Oulu2026-alueen perinneruokareseptejä reseptipankista, opi lisää Oulu2026-alueesta kaupunkikohtaisilta sivulta tai suunnista alueen ruokakulttuuriin kartan avulla.</Text>
            </View>
            <NavigationBar navigation={navigation}/>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: '#F6F7F1',
    },
    topView: {
        width: '100%',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        position: 'absolute',
        backgroundColor: '#F6F7F1',
    },
    logo1: {
        width: 100,
        height: 47.29,
    },
    icon: {
        width: 30,
        height: 30,
    },
    content: {
        marginTop: 120,
    },
    headertextStyle: {
        fontSize: 20, 
        color: '#001E60', 
        textAlign: 'center',
        marginBottom: 20, 
    },
    bodytextStyle: {
        fontSize: 15,
        alignItems: 'center',
        textAlign: 'center',
        color: '#001E60',
    },
    kartta: {
        width: "100%",
        height: 350,
        marginBottom: 40, 
    },

});
