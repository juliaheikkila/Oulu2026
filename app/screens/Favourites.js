import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import NavigationBar from '../consts/NavigationBar.js';

// viedään navigation parametrina, jotta näkymästä toiseen navigoiminen onnistuu
export default function Favourites ({ navigation }) {

    return (
          // renderöidään turvallien alue, joka kattaa laitteen koko näytön
        <SafeAreaView style={styles.background}>
            <View style={styles.topView}>
                {/* Info-ikoni, jota painamalla navigoidaan Info-näkymään */}
                <TouchableOpacity onPress={() => navigation.navigate("Info")}>
                    <Image style={styles.icon} source={require('../assets/info.png')}/>
                </TouchableOpacity>
                    <Image style={styles.logo1} source={require('../assets/RGB_Oulu2026_gradient_horizontal.png')}/>
                {/* Profiili-ikoni, jota painamalla navigoidaan profiili-näkymään */}
                <TouchableOpacity onPress={() => navigation.navigate("Profiili")}>
                    <Image style={styles.icon} source={require('../assets/profiili.png')}/>
                </TouchableOpacity>
            </View>
             {/* Näytön keskiosa, joka sisältää suosikkireseptit */}
            <View style={styles.content}>
                {/* Näytön otsikko */}
                <Text style={styles.screenName}>Suosikit</Text>
                {/* Viesti joka näytetään, jos käyttäjä ei ole lisännyt vielä yhtään reseptiä suosikiksi */}
                <Text style={styles.guideText}>Et ole vielä lisännyt yhtään reseptiä suosikiksi</Text>
            </View>
                {/* Näytön alaosassa oleva navigointipalkki */}
            <NavigationBar navigation={navigation}/>
        </SafeAreaView>
    );
}
// näkymän tyyleihin vaikuttavat asiat löytyvät alta
const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: '#F6F7F1', // taustaväri sovellukselle
    },
    topView: {
        width: '100%', // leveys prosenteina
        height: 100,   // korkeus pikseleinä
        flexDirection: 'row', // asetetaan lapsielementit riviksi
        justifyContent: 'space-around', // jaetaan rivin sisältö tasaisesti tyhjän tilan kesken
        alignItems: 'center', // määritetään elementin pystysuuntainen sijainti keskelle
        position: 'absolute',
        backgroundColor: '#F6F7F1',
    },
    logo1: {
        width: 100,
        height: 47.29,
    },
    icon: {
        width: 30,
        height: 30,
    },
    content: {
        marginTop: 120, // sisältöalueen ylämarginaali
    },
    screenName: {
        left: 27, 
        fontSize: 20, 
        paddingBottom: 10,
        fontWeight: 'bold', 
        color: '#001E60',
    },
    guideText: {
        fontSize: 16,
        color: '#001E60',
        marginTop: 200,
        alignSelf: 'center', // elementti asetetaan keskelle suhteessa sen sisältävään elementtiin
    },

});
