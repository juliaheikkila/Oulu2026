import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity,} from 'react-native';

// Alapalkin toiminnallisuus sekä tyyliasetukset, jotka viedään muihin näkymiin import-toiminnolla.
const NavigationBar = ({navigation}) => {

    return (
    <View style={styles.bottomBar}>
        {/* Painike, jolla navigoidaan Reseptit-näyttöön. */}
        <TouchableOpacity onPress={() => navigation.navigate("Reseptit")}>
            <Image style={{width: 22.91, height: 30, alignSelf: 'center'}} source={require('../assets/resepti_ikoni.png')}/>
            <Text style={styles.iconText}>Reseptit</Text>
        </TouchableOpacity>
        {/* Painike, jolla navigoidaan Kaupungit-näyttöön. */}
        <TouchableOpacity onPress={() => navigation.navigate("Kaupungit")}>
            <Image style={{width: 30, height: 30, alignSelf: 'center'}} source={require('../assets/kaupungit_ikoni.png')}/>
            <Text style={styles.iconText}>Kaupungit</Text>
        </TouchableOpacity>
        {/* Painike, jolla navigoidaan Koti-näyttöön. */}
        <TouchableOpacity onPress={() => navigation.navigate("Koti")}>
            <Image style={{width: 32.67, height: 30, alignSelf: 'center'}} source={require('../assets/koti_ikoni.png')}/>
            <Text style={styles.iconText}> Koti</Text>
        </TouchableOpacity>
        {/* Painike, jolla navigoidaan Kartta-näyttöön. */}
        <TouchableOpacity onPress={() => navigation.navigate("Kartta")}>
            <Image style={{width: 30, height: 30, alignSelf: 'center'}} source={require('../assets/kartta_ikoni.png')}/>
            <Text style={styles.iconText}>Kartta</Text>
        </TouchableOpacity>
        {/* Painike, jolla navigoidaan Suosikit-näyttöön. */}
        <TouchableOpacity onPress={() => navigation.navigate("Suosikit")}>
            <Image style={{width: 32.44, height: 30, alignSelf: 'center'}} source={require('../assets/suosikit_ikoni.png')}/>
            <Text style={styles.iconText}>Suosikit</Text>
        </TouchableOpacity>
    </View>
)}

export default NavigationBar;

const styles = StyleSheet.create({

    bottomBar: {
        width: '100%', // Asetetaan alapalkin leveys vastaamaan koko näytön leveyttä
        paddingTop: 15, // Asetetaan painikkeiden ja alapalkin yläreunan väliin tilaa
        backgroundColor: '#001E60',
        flexDirection: 'row', // Asetetaan alapalkin elementit riviin
        justifyContent: 'space-evenly', // Asetetaan alapalkin elementit jakautumaan tasaisesti sivusuunnassa 
        alignItems: 'stretch',
        position: 'absolute',
        bottom: 0, // Asetetaan alapalkki näytön alaosaan
        // Asetetaan alapalkin korkeudeksi joko 12% tai 10% riippuen käyttöjärjestelmästä.
        ...Platform.select({
            ios: {
                height: '12%',
            },
            android: {
                height: '10%',
            }
        })
    },

    // Alapalkin ikonien tekstien tyyliasetukset
    iconText: {
        fontSize: 12, 
        color: 'white',
        paddingVertical: 5,
    },

});
