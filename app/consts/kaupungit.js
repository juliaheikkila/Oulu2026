const kaupungit = [
    {
        id: '1',
        nimi: 'Kajaani',
        kuvaus: "Kajaanissa pääset luontoon jo kotiovelta ja kaikki tarvittavat palvelut löytyvät kävelymatkan etäisyydeltä. Onkin todistettu, että Kajaanissa vuorokauteen mahtuu 26 tuntia. Kajaani on Kainuun maakuntakeskus ja kaupungin vahvuuksia ovat korkea kansainvälinen osaamistaso, kattava kulttuuritarjonta sekä monipuoliset liikunta- ja harrastusmahdollisuudet.",
        linkki: 'https://www.kajaani.fi/oulu2026/',
        image: require('../assets/Kajaani.jpg'),
        latitude: 64.2240872,
        longitude: 27.7334227,
        perinneruoka: 'Muikkukeitto',
        valmistusaika: '60 min',
        perinneruokakuvaus: "Herkullinen muikkukeitto nimettiin Kajaanin pitäjäruoaksi 1980-luvulla!",
        perinneruokakuva: require('../assets/muikkukeitto.jpeg'),
        ainekset: '1/2 kg muikkuja\n 1/2 kg perunoita\n 1 (50 g) sipuli\n 50 g voita\n 3 rkl ruisjauhoja\n tilkka kylmää vettä\n suolaa',
        valmistus: '1. Perkaa ja huuhdo muikut. Kuori ja lohko perunat. Pane perunat kattilaan ja kaada suolalla maustettua vettä päälle sen verran, että perunat peittyvät. Keitä noin 10 minuuttia, levitä muikut perunoiden päälle ja jatka keittämistä viitisen minuuttia, kunnes sekä perunat että kalat ovat kypsiä.\n 2. Sekoita ruisjauhot vesitilkkaan ja lisää suurus ohuena nauhana kattilaan. Kiehauta keitto.\n 3. Hienonna sipuli. Sulata voi ja lisää sipuli voisulan joukkoon. Tarjoa voi-sipulikastike keiton lisäkkeenä.',
    },

    {
        id: '2',
        nimi: 'Kalajoki',
        kuvaus: "Kalajoella riittää kulttuuritarjontaa ja nähtävää. Plassi, Kalajoen vanha kaupunki, on vierailun arvoinen kohde kylämiljöineen ja museoineen. Keramiikka Isopahkala ja Kahvila Seili taide ovat toimineet jo vuodesta 1972 ja tarjoaa opastettuja kierroksia. Kalajoen Hiekkasärkät keräävät vuosittain tuhansia vierailijoita muun muassa kesäteatterin sekä juhannus festivaalien merkeissä.",
        image: require('../assets/Kalajoki.jpg'),
        linkki: 'https://kalajoki.fi/',
        latitude: 64.260017,
        longitude: 23.950545,
        perinneruoka: 'Klimppisoppa',
        valmistusaika: '50 min',
        perinneruokakuvaus: "Klimppisoppa on 1800-luvun lopulta peräisin oleva eteläpohjalainen ja satakuntalainen perinneruoka. Klimppisoppa valmistetaan naudan luista, lavasta tai potkasta, vedestä, mausteista ja kasviksista. Aineksia keitetään noin kaksi tuntia, kunnes lihat ovat kypsiä, sen jälkeen liemi siivilöidään.",
        perinneruokakuva: require('../assets/klimppisoppa.png'),
        ainekset: '500 g keittolihaa\n 2 l vettä\n 1 sipuli\n 1 tl meiramia\n 2 tl suolaa\n 6 maustepippuria\n Persiljaa\n 20 g voita\n 2,5 dl maitoa\n 4,5 dl vehnäjauhoja\n 1 kananmuna\n 2 tl sokeria\n 0,5 tl suolaa',
        valmistus: '1. Kaada vesi kattilaan ja lisää liha. Kiehauta ja kaavi pois muodostuva vaahto. Kun vaahtoa ei enää synny, lisää mausteet ja sipuli.\n 2. Keitä kunnes liha pehmenee, vajaan tunnin verran. Poista liha liemestä ja pilko pienemmiksi palasiksi.\n 4. Siivilöi pois mausteet ja sipuli. Sekoita klimppiainekset keskenään taikinaksi. Pyörittele lusikalla klimppejä kiehyvaan keittoliemeen.\n 5. Lisää klimppien tekemisen jälkeen leikellyt lihat takaisin keittoon.',

    },

    {
        id: '3',
        nimi: 'Kemi',
        kuvaus: "Kemin kaupunki on ollut villisti kaupunki ja rohkeasti reunalla satama-, teollisuus- ja kulttuurikaupunkina jo vuodesta 1869. Kemiläisillä riittää innovaatioita, joiden helmiä ovat mm. LumiLinna, matkailujäänmurtaja Sampo sekä maailman pohjoisin sarjakuvanäyttely. Kaupungista löytyy myös mm. Lapin ensimmäinen taidemuseo, historiallinen museo, teatteri ja orkesteri. Kulttuurikaupunkivuotta varten kehitämme kulttuurin, matkailun ja teollisuusmatkailun yhteistyötä ja palveluja useilla projekteilla.",
        image: require('../assets/Kemi.jpg'),
        linkki: 'https://www.kemi.fi/vapaa-aika-ja-kulttuuri/kulttuuri/oulu2026/',
        latitude: 65.7333404,
        longitude: 24.5666495,
        perinneruoka: 'Mutti',
        valmistusaika: '1h 30 min',
        perinneruokakuvaus: "Mutti on etenkin Keski- ja Pohjois-Pohjanmaalla suosittu perinneruoka, joka valmistetaan ohrajauhosta ja vedestä sekä maustetaan suolalla. Sitä pidetään vanhimpana ja yksinkertaisimpana pohjoispohjalaisena perinneruokana.",
        perinneruokakuva: require('../assets/mutti.png'),
        ainekset: '0,7 l vettä\n 2-3 tl suolaa\n 600 g ohrajauhoja\n Kirnuvoita',
        valmistus: '1. Vettä kiehautetaan ja suolataan runsaasti. Ohrajauhoja kaadetaan joukkoon yhdellä kerralla niin paljon, että vesi imeytyy jauhoihin ja muodostuu noin herneen kokoisia kokkareita. Juuri jauhojen kaatamisen aikana ei saa hämmentää, sillä silloin tulee seos puuromaiseksi.\n 2. Hämmentämistä hiljaisella tulella jatketaan, kunnes mutti on vaaleanruskeata.\n 3. Mutti nautitaan talkkunan tapaan maidon tai nuoren piimän kanssa, mutta voin tai sokerin kanssa se on myös hyvää.',

    },

    {
        id: '4',
        nimi: 'Oulu',
        kuvaus: "Oulun tunnistaa huisin laadukkaasta arjen elämästä, kaikille avoimista työ- ja kehittymismahdollisuuksista maailman huipulle asti sekä investointi-, innovaatio- ja yrityskulttuurista, joka vie mennessään. Oulu on oivaltava, juureva ja pohjoisen luonnon ehdoilla elävä kaupunki, jossa osaajat ja merkitykselliset tulevaisuuden mahdollisuudet kohtaavat. Oulussa on tilaa, aikaa kohtaamisille, jotka voivat myllertää koko elämäsi – asteen verran paremmin, tietenkin.",
        image: require('../assets/oulu.jpg'),
        linkki: 'https://oulu.com',
        latitude: 65.01236,
        longitude: 25.46816,
        perinneruoka: 'Rössypottu',
        valmistusaika: '60 min',
        perinneruokakuvaus: "Rössypottu on veripaltusta eli rössystä ja palvikyljestä keitetty maukas soppa. Lusikoi perinneherkkua ja haukkaa palan painikkeeksi ruisleipää!",
        perinneruokakuva: require('../assets/rossypottu.png'),
        ainekset: '10 (550 g) kiinteää perunaa (vihreä pussi)\n 1 (75 g) porkkana\n 1 (50 g) sipuli\n 200 g veripalttua (esim. Tapolan)\n 200 g palvikylkeä\n 1 l vettä\n 1 1/2–2 tl suolaa\n 5 maustepippuria\n 2 rkl tummaa siirappia',
        valmistus: '1. Kuori ja paloittele perunat ja porkkana. Hienonna kuorittu sipuli. Kuori veripalttu, jos siinä on kuori ja kuutioi veripalttu ja palvikylki. Ruskista palvikylkeä hetki kattilassa. Siirrä liha sivuun odottamaan. Lisää kattilaan sipulit. Kuullota sipulit läpikuultaviksi. Lisää perunat ja porkkanat.\n 2. Lisää vesi, suola ja maustepippurit. Anna keiton kiehua hiljalleen kunnes perunat ovat kypsiä, noin 10–15 minuuttia.\n 3. Lisää kattilaan siirappi, palviliha ja rössykuutiot. Kuumenna keitto nopeasti.\n 4. Tarkista maku ja lisää tarvittaessa suolaa. Tarjoa keitto ruisleivän kanssa.',

    },
];

export default kaupungit;