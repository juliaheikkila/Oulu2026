import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import StartingScreen from './app/screens/StartingScreen';
import WelcomeScreen from './app/screens/WelcomeScreen';
import Home from './app/screens/Home';
import Recipes from './app/screens/Recipes';
import RecipeDetails from './app/screens/RecipeDetails';
import Towns from './app/screens/Towns';
import TownDetails from './app/screens/TownDetails';
import Map from './app/screens/Map';
import Favourites from './app/screens/Favourites';
import Info from './app/screens/Info';
import Profile from './app/screens/Profile';
import Registration from './app/screens/Registration';
import ProfileInfo from './app/screens/ProfileInfo';
import ProfileSettings from './app/screens/ProfileSettings';
import PasswordChange from './app/screens/PasswordChange';
import ForgotPassword from './app/screens/ForgotPassword';
import VerificationCode from './app/screens/VerificationCode';
import VerificationCodeInfo from './app/screens/VerificationCodeInfo';

// Luodaan Stack-olio, jolla mahdollistetaan sovelluksessa navigointi näkymästä toiseen
const Stack = createNativeStackNavigator();

// App funktiosta palautetaan NavigationContainer komponentti, joka ympäröi Stack.Navigator komponentin
export default function App() {

  return (
    <NavigationContainer>
      <Stack.Navigator>
        {/* Kukin Stack.Screen edustaa yhtä näkymää. Nimeen sijoitetaan kyseisen näkymän nimi. */}
        {/* Component-muuttujaan sijoitetaan importilla tuotu luokan nimi. */}
        <Stack.Screen
          name="Aloitus"
          component={StartingScreen}
        />
        
        <Stack.Screen
          name="Tervetuloa"
          component={WelcomeScreen}
        />

        <Stack.Screen
          name="Koti"
          component={Home}
        />

        <Stack.Screen
          name="Reseptit"
          component={Recipes}
        />

        <Stack.Screen
          name="Reseptisivu"
          component={RecipeDetails}
        />

        <Stack.Screen
          name="Kaupungit"
          component={Towns}
        />

        <Stack.Screen
          name="Kaupunkisivu"
          component={TownDetails}
        />

        <Stack.Screen
          name="Kartta"
          component={Map}
        />

        <Stack.Screen
          name="Suosikit"
          component={Favourites}
        />

        <Stack.Screen
          name="Info"
          component={Info}
        />
        
        <Stack.Screen
          name="Profiili"
          component={Profile}
        />

        <Stack.Screen
          name="Rekisteröidy"
          component={Registration}
        />

        <Stack.Screen
          name="Profiilitiedot"
          component={ProfileInfo}
        />

        <Stack.Screen
          name="Käyttäjän asetukset"
          component={ProfileSettings}
        />
       
        <Stack.Screen
          name="Salasanan vaihto"
          component={PasswordChange}
        />

        <Stack.Screen
          name="Unohtuiko salasana"
          component={ForgotPassword}
        />

        <Stack.Screen
          name="Varmistuskoodi"
          component={VerificationCode}
        />

        <Stack.Screen
          name="Varmistuskoodi info"
          component={VerificationCodeInfo}
        />
        
      </Stack.Navigator>
    </NavigationContainer>
  )

}
